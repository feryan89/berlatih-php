<?php
function tukar_besar_kecil($string){
//$kalimat=explode(" ",$string);
$newKalimat="";
//for($i=0;$i<count($kalimat);$i++){
	for($j=0;$j<strlen($string);$j++){
		$karakter=substr($string,$j,1);
		if(ctype_upper($karakter)){
			$karakter=strtolower($karakter);
		}else{
			$karakter=strtoupper($karakter);
		}
		$newKalimat.=$karakter;
	}
	//$newKalimat.=" ";
//}
return $newKalimat."<br />";
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>