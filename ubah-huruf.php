<?php
function ubah_huruf($string){
//kode di sini
$asciiChar="";
$newKata="";
	for($i=0;$i<strlen($string);$i++){
		$karakter=substr($string,$i,1);
		if(ord($karakter)==122){
			$asciiChar=97;
		}else{
			$asciiChar=ord($karakter)+1;
		}
		$newKata.=chr($asciiChar);
	}
	return $newKata."<br />";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>