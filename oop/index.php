<?php
require_once "animal.php";
require_once "Ape.php";
require_once "Frog.php";

$sheep = new Animal("shaun");

echo $sheep->name."<br/>"; // "shaun"
echo $sheep->legs."<br/>"; // 2
$sheep->get_cold_blooded(); // false


$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"
?>