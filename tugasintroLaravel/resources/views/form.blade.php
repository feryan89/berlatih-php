<!DOCTYPE html>
<html>
    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <form method="get" action="/welcome">
            @csrf
            <p>First Name:</p>
            <p><input type="text" name="txtNama1" /></p>
            
            <p>Last Name:</p>
            <p><input type="text" name="txtNama2" /></p>

            <p>Gender :</p>
            <p>
                <input type="radio" name="gender" value="1"/>Male<br />
                <input type="radio" name="gender" value="2"/>Female<br />
                <input type="radio" name="gender" value="3"/>Other<br />
            </p>
            <p>Nationality:</p>
            <p>
                <select name="negara">
                    <option value="indo" selected="selected">Indonesian</option>
                    <option value="singa" >Singapore</option>
                    <option value="malay" >Malaysian</option>
                    <option value="australi">Australian</option>
                </select>
            </p>
            <p>Language Spoken:</p>
                <input type="checkbox" name= "bahasa[]" value="indo"/> Bahasa Indonesia <br/>
                <input type="checkbox" name= "bahasa[]" value="indo"/> English <br/>
                <input type="checkbox" name= "bahasa[]" value="indo"/> Other <br/>
            </p>
            <p>Bio:</p>
            <p>
                <textarea name="txtBio" rows="10" cols="30"></textarea>
            </p>
            <input type="submit" name="submit" value="SignUp" />
        </form>
    </body>
</html>